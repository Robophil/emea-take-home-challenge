import React from "react";
import { MemoryRouter } from 'react-router-dom';
import { createShallow } from '@material-ui/core/test-utils';

import BookCard, { Book } from '../../components/BookCard';

describe('<BookCard />', () => {
  let book: Book;
  let addToCart: any;
  let shallow: any;

  beforeEach(() => {
    book = {
      "Title": "Superfreakonomics",
      "Author": "Dubner, Stephen",
      "Genre": "science",
      "SubGenre": "economics",
      "Height": "179",
      "Publisher": "HarperCollins",
      "Id": 3
      } as Book;
    addToCart = jest.fn();
    shallow = createShallow();
  })

  it('should render', () => {
    const wrapper = shallow(
      <MemoryRouter>
        <BookCard book={book} addToCart={addToCart}/>
      </MemoryRouter>
    );
    expect(wrapper.find(BookCard)).toHaveLength(1);
  })

})
