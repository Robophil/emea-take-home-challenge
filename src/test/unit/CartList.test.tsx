import React from "react";
import { MemoryRouter } from 'react-router-dom';
import { createShallow } from '@material-ui/core/test-utils';

import { Book } from '../../components/BookCard';
import { CartItemStore } from '../../components/CartItem';
import CartList from '../../components/CartList';

describe('<CartItem />', () => {
  let books: Book[];
  let cart: CartItemStore
  let addToCart: any;
  let removeFromCart: any;
  let shallow: any;

  beforeEach(() => {
    books = [{
      "Title": "Superfreakonomics",
      "Author": "Dubner, Stephen",
      "Genre": "science",
      "SubGenre": "economics",
      "Height": "179",
      "Publisher": "HarperCollins",
      "Id": 3
      }] as Book[];
    cart = {
      '3': 4
    } as CartItemStore
    addToCart = jest.fn();
    removeFromCart = jest.fn();
    shallow = createShallow();
  })

  it('should render', () => {
    const wrapper = shallow(
      <MemoryRouter>
        <CartList books={books}
          cart={cart}
          addToCart={addToCart}
          removeFromCart={removeFromCart}/>
      </MemoryRouter>
    );
    expect(wrapper.find(CartList)).toHaveLength(1);
  })

})

