import React from "react";
import { MemoryRouter } from 'react-router-dom';
import { createShallow } from '@material-ui/core/test-utils';

import { Book } from '../../components/BookCard';
import BookDetails from '../../components/BookDetails';

describe('<BookDetails />', () => {
  let book: Book;
  let addToCart: any;
  let shallow: any;

  beforeEach(() => {
    book = {
      "Title": "Superfreakonomics",
      "Author": "Dubner, Stephen",
      "Genre": "science",
      "SubGenre": "economics",
      "Height": "179",
      "Publisher": "HarperCollins",
      "Id": 3
      } as Book;
    addToCart = jest.fn();
    shallow = createShallow();
  })

  it('should render', () => {
    const wrapper = shallow(
      <MemoryRouter>
        <BookDetails book={book} addToCart={addToCart}/>
      </MemoryRouter>
    );
    expect(wrapper.find(BookDetails)).toHaveLength(1);
  })

})
