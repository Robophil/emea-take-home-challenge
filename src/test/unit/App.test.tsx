import React from "react";
import { MemoryRouter } from 'react-router-dom';
import { createShallow } from '@material-ui/core/test-utils';

import App from '../../components/App';

describe('<BookCard />', () => {
  let shallow: any;

  beforeEach(() => {
    shallow = createShallow();
  })

  it('should render', () => {
    const wrapper = shallow(
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );
    expect(wrapper.find(App)).toHaveLength(1);
  })

})
