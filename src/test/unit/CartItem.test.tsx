import React from "react";
import { MemoryRouter } from 'react-router-dom';
import { createShallow } from '@material-ui/core/test-utils';

import { Book } from '../../components/BookCard';
import CartItem from '../../components/CartItem';

describe('<CartItem />', () => {
  let book: Book;
  let addToCart: any;
  let removeFromCart: any;
  let shallow: any;

  beforeEach(() => {
    book = {
      "Title": "Superfreakonomics",
      "Author": "Dubner, Stephen",
      "Genre": "science",
      "SubGenre": "economics",
      "Height": "179",
      "Publisher": "HarperCollins",
      "Id": 3
      } as Book;
    addToCart = jest.fn();
    removeFromCart = jest.fn();
    shallow = createShallow();
  })

  it('should render', () => {
    const quantity = 4;
    const wrapper = shallow(
      <MemoryRouter>
        <CartItem book={book}
          quantity={quantity}
          addToCart={addToCart}
          removeFromCart={removeFromCart}/>
      </MemoryRouter>
    );
    expect(wrapper.find(CartItem)).toHaveLength(1);
  })

})

