import React from "react";
import { MemoryRouter } from 'react-router-dom';
import { createShallow } from '@material-ui/core/test-utils';

import { Book } from '../../components/BookCard';
import BookList from '../../components/BookList';

describe('<BookList />', () => {
  let books: Book[];
  let addToCart: any;
  let shallow: any;

  beforeEach(() => {
    books = [{
      "Title": "Superfreakonomics",
      "Author": "Dubner, Stephen",
      "Genre": "science",
      "SubGenre": "economics",
      "Height": "179",
      "Publisher": "HarperCollins",
      "Id": 3
      }] as Book[];
    addToCart = jest.fn();
    shallow = createShallow();
  })

  it('should render', () => {
    const wrapper = shallow(
      <MemoryRouter>
        <BookList books={books} addToCart={addToCart}/>
      </MemoryRouter>
    );
    expect(wrapper.find(BookList)).toHaveLength(1);
  })

})
