import 'jest-localstorage-mock';
import { renderHook, act } from '@testing-library/react-hooks'

import { useManageCart } from '../../hooks/useManageCart';
import { CartItemStore } from '../../components/CartItem';

describe('useManageCart', () => {
  afterEach(() => {
    localStorage.clear();
    jest.clearAllMocks();
  });

  it('should add item to cart twice', () => {
    const { result } = renderHook(() => useManageCart({}));
    const bookId = '1';
    let quantity = 0;
    expect(result.current[0]).toMatchObject({});
    act(() => {
      // addToCart
      result.current[1](bookId);
    })

    quantity = result.current[0][bookId];
    expect(quantity).toBe(1);

    act(() => {
      // addToCart
      result.current[1](bookId);
    })

    quantity = result.current[0][bookId];
    expect(quantity).toBe(2);
  })

  it('should remove items from cart', () => {
    const bookId = '1';
    const init = {
      [bookId]: 2
    } as CartItemStore;
    const { result } = renderHook(() => useManageCart(init));

    let quantity = 0;
    expect(result.current[0]).toMatchObject(init);
    act(() => {
      // removeFromCart
      result.current[2](bookId);
    })

    quantity = result.current[0][bookId];
    expect(quantity).toBe(1);

    act(() => {
      // removeFromCart
      result.current[2](bookId);
    })

    quantity = result.current[0][bookId];
    expect(quantity).toBeUndefined();
  })

})
