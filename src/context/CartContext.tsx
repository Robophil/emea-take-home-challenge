import React, { createContext, ReactNode } from 'react';

import { useManageCart } from '../hooks/useManageCart';
import { CartItemStore } from '../components/CartItem';

type Props = {
  children: ReactNode
};
export const CartContext = createContext([]);
export const CartContextProvider = (props: Props) => {
  const [cart, addToCart, removeFromCart] = useManageCart({} as CartItemStore);
  return (
    <CartContext.Provider value={[cart, addToCart, removeFromCart] as any}>
    	{props.children}
    </CartContext.Provider>
  );
}
