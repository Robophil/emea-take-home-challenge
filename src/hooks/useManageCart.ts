import { useLocalStorage } from 'react-use';

import { CartItemStore } from '../components/CartItem';

export function useManageCart(initCart: CartItemStore): [CartItemStore, (bookId: string) => void,  (bookId: string) => void] {
  const [cart, setCart] = useLocalStorage<CartItemStore>('cart', initCart);

  const addToCart = (bookId: string) => {
    const newCart = {...cart};
    const quantity = newCart[bookId] ?? 0;
    const newQuantity = quantity + 1;
    newCart[bookId] = newQuantity;

    setCart(newCart);
  }
  const removeFromCart = (bookId: string) => {
    const newCart = {...cart};
    const quantity = newCart[bookId] ?? 0;
    const newQuantity = quantity - 1;

    if (newQuantity < 1) {
      delete newCart[bookId]
    } else {
      newCart[bookId] = newQuantity;
    }
    setCart(newCart);
  }

  return [cart!, addToCart, removeFromCart];
}
