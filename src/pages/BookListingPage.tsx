import React, { useEffect, useState, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import Alert from '@material-ui/lab/Alert';
import PaginationItem from '@material-ui/lab/PaginationItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  useLocation,
  Link,
} from "react-router-dom";

import { Book, BookPagination } from '../components/BookCard';
import BookList from '../components/BookList';

import { CartContext } from '../context/CartContext';

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

const useStyles = makeStyles(() => ({
  progress: {
    marginTop: '40vh'
  },
  pagination: {
    marginBottom: 10
  }
}));

export default function BookListingPage() {
  const [books, setBooks] = useState<Book[]>([]);
  const [error, setError] = useState<Error>();
  const [, addToCart ] = useContext(CartContext);
  const [bookPagination, setBookPagination] = useState<BookPagination>({} as BookPagination);
  const [showProgress, setShowProgress] = useState<boolean>(true);

  const query = useQuery();
  const classes = useStyles();
  const currentPage = +(query.get('currentPage') || 1);

  async function fetchBooks(page: number) {
    setShowProgress(true);
    try {
      await fetch(`http://localhost:3000/api/books/chunk/${page}`)
        .then(res => res.json())
        .then(json => {
          setBooks(json.books || []);
          setBookPagination(json);
        });
    } catch(err) {
      console.error(err);
      setError(err);
    }
    setShowProgress(false);
  }

  useEffect(() => {
    fetchBooks(currentPage - 1);
  }, [currentPage]);

  if (error && error.message) {
    return (
      <Alert severity="error">
        {`
          An error occured: ${error.message}.
          Reload to try again :(
        `}
      </Alert>
    );
  }

  return (
    <>
    {showProgress ? <CircularProgress className={classes.progress} /> : <>
      <BookList books={books} addToCart={addToCart}/>
      <Pagination
        className={classes.pagination}
        page={currentPage}
        count={bookPagination.maxPage}
        renderItem={(item) => (
          <PaginationItem
            component={Link}
            to={`/?currentPage=${item.page}`}
            {...item}
          />
        )}
      />
    </>
    }
    </>
  );
}
