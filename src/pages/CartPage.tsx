import React, { useEffect, useState, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';
import { usePrevious } from 'react-use';

import CartList from '../components/CartList';
import { Book } from '../components/BookCard';

import { CartContext } from '../context/CartContext';

const useStyles = makeStyles(() => ({
  progress: {
    marginTop: '40vh'
  }
}));

export default function CartPage() {
  const [showProgress, setShowProgress] = useState<boolean>(true);
  const [ cart, addToCart, removeFromCart ] = useContext(CartContext);
  const [books, setBooks] = useState<Book[]>([]);
  const [error, setError] = useState<Error>();
  const [ bookIdsInCart, setBookIdsInCart ] = useState<string[]>(Object.keys(cart as any));
  const prevBookIdsInCart = usePrevious<string[]>(bookIdsInCart);
  const classes = useStyles();

  async function fetchBooks(bookIds: string[]): Promise<void> {
    setShowProgress(true);
    try {
      const booksInCart: Book[] = await Promise.all(
        bookIds.map((bookId: string) => fetch(`http://localhost:3000/api/books/${bookId}`)
        .then(res => res.json()))
      )
      setBooks(booksInCart);
    } catch(err) {
      console.error(err);
      setError(err);
    }
    setShowProgress(false);
  }

  useEffect(() => {
    const currentBookIdsInCart = Object.keys(cart as any);
    const exist = prevBookIdsInCart && currentBookIdsInCart.some(bookId => prevBookIdsInCart!.includes(bookId));
    if (!exist) {
      fetchBooks(currentBookIdsInCart);
    } else {
      const newBooks = books.filter(book => cart[book.Id]);
      setBooks(newBooks);
    }
    setBookIdsInCart(currentBookIdsInCart);
  }, [cart]);

  if (error && error.message) {
    return (
      <Alert severity="error">
        {`
          An error occured: ${error.message}.
          Reload to try again :(
        `}
      </Alert>
    );
  }

  return (
    <>
    {showProgress ? <CircularProgress className={classes.progress} /> : (
      books && books.length > 0 ?
      <CartList
        books={books}
        cart={cart}
        addToCart={addToCart}
        removeFromCart={removeFromCart}
      /> : <p>Cart is currently empty :(</p>
      )
    }
    </>
  );
}
