import React, { useEffect, useState, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';
import {
  useParams
} from "react-router-dom";

import BookDetails from '../components/BookDetails';
import { Book } from '../components/BookCard';

import { CartContext } from '../context/CartContext';

const useStyles = makeStyles(() => ({
  progress: {
    marginTop: '40vh'
  }
}));

type Params = {
  bookId: string
};

export default function BookDetailsPage() {
  const [showProgress, setShowProgress] = useState<boolean>(true);
  const [book, setBook] = useState<Book>({} as Book);
  const [error, setError] = useState<Error>();
  const [, addToCart ] = useContext(CartContext);

  const { bookId } = useParams<Params>();
  const classes = useStyles();

  async function fetchBookDetails() {
    setShowProgress(true);
    try {
      await fetch(`http://localhost:3000/api/books/${bookId}`)
        .then(res => res.json())
        .then(json => {
          setBook(json);
        });
    } catch(err) {
      console.error(err);
      setError(err);
    }
    setShowProgress(false);
  }

  useEffect(() => {
    fetchBookDetails();
  }, []);

  if (error && error.message) {
    return (
      <Alert severity="error">
        {`
          An error occured: ${error.message}.
          Reload to try again :(
        `}
      </Alert>
    );
  }

  return (
    <>
    {showProgress ? <CircularProgress className={classes.progress} /> : <BookDetails book={book} addToCart={addToCart}/>}
    </>
  );
}
