import React from 'react';
import ReactDOM from 'react-dom';
import {
    BrowserRouter as Router,
} from "react-router-dom";

import { CartContextProvider } from './context/CartContext';
import App from './components/App';

ReactDOM.render((
  <Router>
    <CartContextProvider>
      <App/>
    </CartContextProvider>
  </Router>
), document.getElementById('root'));
