import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import {
  Link,
} from "react-router-dom";

const cardStyles = makeStyles({
  root: {
    minWidth: '50%',
    marginBottom: 8,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export interface Book {
  Id: number;
  Title: string;
  Author: string;
  Genre: string;
  SubGenre: string;
  Height: string;
  Publisher: string;
}

export interface BookPagination {
  books: Book[] | undefined;
  total: number;
  maxPage: number;
  currentPage: number
}

type Props = {
  book: Book,
  addToCart: (bookId: string) => void
};

export default function BookCard(props: Props) {
  const classes = cardStyles();
  const { book, addToCart } = props;

  return (
    <Card key={book.Title} className={classes.root}>
      <CardContent>
        <Typography variant="h5" component="h2">
          {book.Title}
        </Typography>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {book.Author}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">
          <Link to={`/book-details/${book.Id}`}>Book in details</Link>
        </Button>
        <Button size="small" onClick={() => addToCart(''+book.Id)}>Add to cart</Button>
      </CardActions>
    </Card>
  );
}
