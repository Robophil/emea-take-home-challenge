import React from 'react';
import BookCard, { Book } from './BookCard';

type Props = {
  books: Book[],
  addToCart: (bookId: string) => void
};

export default function BookList(props: Props) {
  const { books, addToCart } = props;
  return (
    <>
      {books.map((book: Book) => {
        return (
          <BookCard key={book.Title} book={book} addToCart={addToCart} />
        )}
      )}
    </>
  )
}
