import React from 'react';
import { Book } from './BookCard';
import CartItem, { CartItemStore} from './CartItem';

type Props = {
  books: Book[],
  cart: CartItemStore,
  addToCart: (bookId: string) => void,
  removeFromCart: (bookId: string) => void
};

export default function CartList(props: Props) {
  const { books, cart, addToCart, removeFromCart } = props;
  return (
    <>
      {books.map((book: Book) => {
        const quantity = cart[''+book.Id]
        return (
          <CartItem key={book.Title}
            book={book}
            addToCart={addToCart}
            removeFromCart={removeFromCart}
            quantity={quantity} />
        )
      })}
    </>
  )
}
