import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import CssBaseline from '@material-ui/core/CssBaseline';
import {
    Switch,
    Route,
    Redirect,
    Link
} from "react-router-dom";

import BookDetailsPage from '../pages/BookDetailsPage';
import BookListingPage from '../pages/BookListingPage';
import CartPage from '../pages/CartPage';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  progress: {
    marginTop: '40vh'
  },
  link: {
    color: 'inherit',
    textDecoration: 'inherit'
  }
}));

export default function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            <Link to="/" className={classes.link}>Book Shop</Link>
          </Typography>
          <Button color="inherit">
          <Link to="/cart" className={classes.link}>Cart</Link>
          </Button>
        </Toolbar>
      </AppBar>
      <Toolbar/>
      <main className={classes.content}>
        <Switch>
          <Route exact path="/">
            <BookListingPage/>
          </Route>
          <Route path="/book-details/:bookId">
            <BookDetailsPage/>
          </Route>
          <Route path="/cart">
            <CartPage/>
          </Route>
          <Route path='/404' component={() => <p>404 route :(</p>} />
          <Redirect from='*' to='/404' />
        </Switch>
      </main>
    </div>
  );
}
