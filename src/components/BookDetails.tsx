import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import { Book } from './BookCard';

const cardStyles = makeStyles({
  root: {
    minWidth: '50%',
    marginBottom: 8,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

type Props = {
  book: Book,
  addToCart: (bookId: string) => void
};

export default function BookDetails(props: Props) {
  const classes = cardStyles();
  const { book, addToCart } = props;

  return (
    <Card key={book.Title} className={classes.root}>
      <CardContent>
        <Typography variant="h5" component="h2">
          {book.Title}
        </Typography>
        <List>
          <ListItem>
            <ListItemText
              primary="Author: "
              secondary={book.Author}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Genre: "
              secondary={book.Genre}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Sub Genre: "
              secondary={book.SubGenre}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Book Height: "
              secondary={book.Height}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Publisher: "
              secondary={book.Publisher}
            />
          </ListItem>
        </List>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={() => addToCart(''+book.Id)}>Add to cart</Button>
      </CardActions>
    </Card>
  );
}
