import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import { Book } from './BookCard';

const cardStyles = makeStyles({
  root: {
    minWidth: '50%',
    marginBottom: 8,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

type Props = {
  book: Book,
  quantity: number,
  addToCart: (bookId: string) => void,
  removeFromCart: (book: string) => void
};

export type CartItemStore = {
  [key: string]: number;
}

export default function CartItem(props: Props) {
  const classes = cardStyles();
  const { book, quantity, addToCart, removeFromCart } = props;

  return (
    <Card key={book.Title} className={classes.root}>
      <CardContent>
        <Typography variant="h5" component="h2">
          {book.Title}
        </Typography>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {book.Author}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={() => removeFromCart(''+book.Id)}>-</Button>
        {quantity}
        <Button size="small" onClick={() => addToCart(''+book.Id)}>+</Button>
      </CardActions>
    </Card>
  );
}
