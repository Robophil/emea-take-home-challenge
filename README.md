EMEA Take Home Challenge
=================

- Install dependencies
    ```bash
    yarn install or npm install
    ```

- Start Server
    Start server app first by running
    ```bash
    yarn server:dev or npm start server:dev
    ```
    
- Start Client app
    Start client app by running
    ```bash
    yarn start or npm start
    ```
    App would be available on `http://localhost:1234`

- Server Tests
    - unit tests `yarn test:server:unit` or `npm run test:server:unit`
    - integration tests `yarn test:server:integration` or `npm run test:server:integration`
    - e2e tests `yarn test:server:e2e` or `npm run test:server:e2e`

- Client Tests
    - unit tests `yarn test:client:unit` or `npm run test:client:unit`
    - integration tests `yarn test:client:integration` or `npm run test:client:integration`

- Build React bundle in `/dist`
    ```bash
     yarn build or npm run build
    ```

- Architecture
   
    This application is made up of a React app and a NodeJs application.
    It has been bootstraped with Typescript to help with type checking and catching error pretty early.
