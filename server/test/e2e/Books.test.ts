import request from 'supertest';
import { server } from './setupServer';

describe('api/books', () => {
  let oneBook: any;

  beforeAll(() => {
    oneBook = {
      "Title": "Superfreakonomics",
      "Author": "Dubner, Stephen",
      "Genre": "science",
      "SubGenre": "economics",
      "Height": "179",
      "Publisher": "HarperCollins",
      "Id": 3
    };
  })

  describe('GET /', () => {
    it('returns all books', async () => {
      expect.assertions(1);
      const response = await request(server.app)
        .get('/api/books')
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body).toHaveLength(211);
    })
  })

  describe('GET /:id', () => {
    it('returns a book', async () => {
      expect.assertions(1);
      const bookId = 3;
      const response = await request(server.app)
        .get(`/api/books/${bookId}`)
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body).toMatchObject(oneBook);
    })

    it('returns 404', async () => {
      expect.assertions(1);
      const bookId = 100000000000000000000;
      const response = await request(server.app)
        .get(`/api/books/${bookId}`)
        .expect('Content-Type', /json/)
        .expect(404);

      expect(response.body.message).toBe('Book not found!');
    })
  })

  describe('GET /:id', () => {
    it('returns a book', async () => {
      expect.assertions(1);
      const bookId = 3;
      const response = await request(server.app)
        .get(`/api/books/${bookId}`)
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body).toMatchObject(oneBook);
    })

    it('returns 404', async () => {
      expect.assertions(1);
      const bookId = 100000000000000000000;
      const response = await request(server.app)
        .get(`/api/books/${bookId}`)
        .expect('Content-Type', /json/)
        .expect(404);

      expect(response.body.message).toBe('Book not found!');
    })
  })

  describe('GET /chunk/:currentPage', () => {
    it('returns pagination with books', async () => {
      expect.assertions(4);
      const currentPage = 0;
      const response = await request(server.app)
        .get(`/api/books/chunk/${currentPage}`)
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body.books).toHaveLength(20);
      expect(response.body.total).toBe(211);
      expect(response.body.maxPage).toBe(11);
      expect(response.body.currentPage).toBe(currentPage);
    })

    it('returns pagination but with books undefined', async () => {
      expect.assertions(4);
      const currentPage = 1000000;
      const response = await request(server.app)
        .get(`/api/books/chunk/${currentPage}`)
        .expect('Content-Type', /json/)
        .expect(200);

      expect(response.body.books).toBeUndefined();
      expect(response.body.total).toBe(211);
      expect(response.body.maxPage).toBe(11);
      expect(response.body.currentPage).toBe(currentPage);
    })
  })

})
