import { BookService } from '../../../../api/services/BookService';

describe('BookService', () => {
  let oneBook: any;
  let bookService: BookService;

  beforeAll(async () => {
    oneBook = {
      "Title": "Superfreakonomics",
      "Author": "Dubner, Stephen",
      "Genre": "science",
      "SubGenre": "economics",
      "Height": "179",
      "Publisher": "HarperCollins",
      "Id": 3
      };
      bookService = new BookService();
  })

  describe('find()', () => {
    it('returns all books', async () => {
      expect.assertions(1);
      const books = await bookService.find();
      expect(books).toHaveLength(211);
    })
  })

  describe('findOne(id)', () => {
    it('returns one book', async () => {
      expect.assertions(1);
      const id = 3;
      const book = await bookService.findOne(id);
      expect(book).toMatchObject(oneBook);
    })

    it('returns undefined', async () => {
      expect.assertions(1);
      const id = 100000000000000000000;
      const book = await bookService.findOne(id);
      expect(book).toBeUndefined();
    })
  })

  describe('findQuery(currentPage)', () => {
    it('returns bookPagination data', async () => {
      expect.assertions(4);
      const currentPage  = 1;
      const bookPagination = await bookService.findQuery(currentPage);
      expect(bookPagination.books).toHaveLength(20);
      expect(bookPagination.total).toBe(211);
      expect(bookPagination.maxPage).toBe(11);
      expect(bookPagination.currentPage).toBe(currentPage);
    })

    it('returns bookPagination.books as undefined', async () => {
      expect.assertions(4);
      const currentPage  = 100000000000000000000;
      const bookPagination = await bookService.findQuery(currentPage);
      expect(bookPagination.books).toBeUndefined();
      expect(bookPagination.total).toBe(211);
      expect(bookPagination.maxPage).toBe(11);
      expect(bookPagination.currentPage).toBe(currentPage);
    })
  })

})
