import { arrayToChunks } from '../../../utils/ArrayUtils';

describe('ArrayUtils', () => {
  it('split array into 3 chunks', () => {
    const array = [1,2,3,4,5,6,7,8,9];
    const chunks = arrayToChunks<number>(array, 3);
    expect(chunks).toHaveLength(3);
  })

  it('split array into 5 chunks', () => {
    const array = [1,2,3,4,5,6,7,8,9];
    const chunks = arrayToChunks<number>(array, 2);
    expect(chunks).toHaveLength(5);
  })
})
