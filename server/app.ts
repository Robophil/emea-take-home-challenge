import { createExpressServer, useContainer } from 'routing-controllers';
import { Container } from 'typedi';

import { BookController } from './api/controllers/BookController';

useContainer(Container);
export const app = createExpressServer({
  routePrefix: '/api',
  cors: true,
  controllers: [BookController]
});
