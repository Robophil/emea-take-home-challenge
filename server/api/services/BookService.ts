import { Service } from 'typedi';
import csvtojson from 'csvtojson';
import path from 'path';

import { arrayToChunks } from '../../utils/ArrayUtils';

export interface Book {
  Id?: number;
  Title: string;
  Author: string;
  Genre: string;
  SubGenre: string;
  Height: string;
  Publisher: string;
}

export interface BookPagination {
  books: Book[] | undefined;
  total: number;
  maxPage: number;
  currentPage: number
}

@Service()
export class BookService {

    private books: Book[] | undefined = undefined;
    private bookChunks: Book[][] | undefined;
    private maxSize = 20;

    public async find(): Promise<Book[]> {
      return this.getBooks();
    }

    public async findOne(id: number): Promise<Book | undefined> {
      const books = await this.getBooks();
      return books[id];
    }

    public async findQuery(currentPage: number) : Promise<BookPagination> {
      const bookChunks = await this.getBookChunks();
      const books = await this.getBooks();

      return {
        books: bookChunks[currentPage],
        total: books.length,
        maxPage: bookChunks.length,
        currentPage
      } as BookPagination;
    }

    private async getBooks(): Promise<Book[]> {
      if (this.books !== undefined) {
        return Promise.resolve(this.books);
      }
      const books = (await csvtojson().fromFile(path.resolve(__dirname, '../../books.csv')))
        .map((book: Book, index: number) => ({...book, Id: index}));
      console.log(`${books.length} books loaded`);
      this.books = books;
      return books;
    }

    private async getBookChunks(): Promise<Book[][]> {
      if (this.bookChunks !== undefined) {
        return Promise.resolve(this.bookChunks);
      }
      const books = await this.getBooks();
      const bookChunks = arrayToChunks<Book>(books, this.maxSize);
      this.bookChunks = bookChunks;
      return bookChunks;
    }

}
