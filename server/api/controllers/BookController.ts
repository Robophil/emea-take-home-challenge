import {
  Get,
  JsonController,
  OnUndefined,
  Param,
} from 'routing-controllers'
import { Service } from 'typedi';

import { BookNotFoundError } from '../errors/BookNotFoundError'
import { BookService, Book, BookPagination } from '../services/BookService'

@JsonController('/books')
@Service()
export class BookController {

  constructor(private bookService: BookService) {}

  @Get()
  public getAll(): Promise<Book[]> {
    return this.bookService.find()
  }

  @Get('/:id')
  @OnUndefined(BookNotFoundError)
  public getOne(@Param('id') id: number): Promise<Book | undefined> {
    return this.bookService.findOne(id)
  }

  @Get('/chunk/:currentPage')
  public getBooksInChunks(@Param('currentPage') currentPage: number): Promise<BookPagination> {
    return this.bookService.findQuery(currentPage)
  }

}
